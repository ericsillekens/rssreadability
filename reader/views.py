from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import HttpResponseNotFound

from readability.readability import Document
from urllib.request import urlopen
from lxml import html

@require_http_methods(["GET", "POST"])
def read(request):
    if request.POST:
        url = request.POST['url']
    elif request.GET:
        url = request.GET['url']
    else:
        return HttpResponseNotFound()
    rawhtml = urlopen(url).read()
    document = Document(rawhtml, negative_keywords="reactie")
    article = html.fromstring(document.summary(html_partial=True))
    #article.rewrite_links(lambda url: url,resolve_base_href=True )
    for link in article.iterlinks():
        if link[0].tag == 'a':
            link[0].set('href','/reader/?url='+link[2])
    context = {
            'title': document.short_title(),
            'aside': url,
            'content': html.tostring(article),
    }
    return render(request, 'reader/read.html', context)
