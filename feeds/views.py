from django.shortcuts import get_object_or_404,render
from .models import Feed
from feedparser import parse

def index(request):
    feed_list = Feed.objects.all()
    context = {'feed_list': feed_list}
    return render(request, 'feeds/index.html', context) 

def detail(request, feed_id):
    feed = get_object_or_404(Feed, pk=feed_id)
    d = parse(feed.url)
    context = {
            'feed': d.feed,
            'entry_list': d.entries,
    }
    return render(request, 'feeds/detail.html', context)

